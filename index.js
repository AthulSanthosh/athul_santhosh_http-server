const express = require('express')
const app = express()
const path = require('path')
const uuid = require('uuid')
const JSONfile = require('./JSonFile')
// start a server on PORT 3000

const PORT = process.env.PORT || 3000

// create a variable in order to return as uuid string
let word = "uuid"

// create a variable for status code 
let status_code = 400

// create a variable for delay in second
let delay_in_sec = 3

// problem 1 read index.html file content

app.get('/html',(req,res) => {
    res.sendFile(path.join(__dirname,"index.html"))
})

// problem 2 , read the JSON file

app.get('/JSON',(req,res) => {
    res.json(JSONfile)
})

// problem 3 , get UUID 

app.get('/uuid',(req,res) => {
    res.send(`uuid : ${uuid.v1(word)}`)
})

// problem4 , get status code

app.get(`/status/${status_code}`,(req,res) => {
    res.send(`${status_code} : ${res.statusCode}`)
})

// problem5 , get response after delayed time in seconds

app.get(`/delay/${delay_in_sec}`,(req,res) => {
    setTimeout(() => {
        res.send(res.statusCode)
    },delay_in_sec * 1000)
})


app.listen(PORT, () => console.log(`Port has started on ${PORT} `))